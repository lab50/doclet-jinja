# Jinja Doclet

This library provides a Doclet for use with the Javadoc tool in order to
export Java documentation with custom Jinja template.

Jinja is a web template engine provides Python-like expressions while
ensuring that the templates are evaluated in a sandbox. It is a text-based
template language and thus can be used to generate any markup as well as sourcecode.

Used [HubSpot Jinja engine](https://github.com/HubSpot/jinjava) witch supports most
features of original engine.

How to use:
```
javadoc -doclet net.lab50.jinjadoclet.Doclet \
    -docletpath jinjadoclet-1.0-jar-with-dependencies.jar \
    -sourcepath <pathlist> [packagenames] -d <directory> \
    -template <file> -out <output file>
```

## Examples

See example directory.