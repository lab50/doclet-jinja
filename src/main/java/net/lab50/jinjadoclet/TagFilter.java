//
// © ООО «Лаборатория 50» 2019
// DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
//
// This code is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License version 3 only, as
// published by the Free Software Foundation.  Oracle designates this
// particular file as subject to the "Classpath" exception as provided
// by Oracle in the LICENSE file that accompanied this code.
//
// This code is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// version 3 for more details (a copy is included in the СOPYING file that
// accompanied this code).
//
// You should have received a copy of the GNU General Public License version
// 2 along with this work; if not, write to the Free Software Foundation,
// Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
//
package net.lab50.jinjadoclet;

import com.hubspot.jinjava.interpret.JinjavaInterpreter;
import com.hubspot.jinjava.interpret.TemplateSyntaxException;
import com.hubspot.jinjava.lib.filter.Filter;
import com.sun.javadoc.Tag;

public class TagFilter implements Filter {

    @Override
    public String getName() {
        return "docbook";
    }

    @Override
    public Object filter(Object var, JinjavaInterpreter interpreter, String ...args) {

        if (!var.getClass().isArray()) {
            throw new TemplateSyntaxException(interpreter, getName(), "Object must be array " + var.getClass().getName());
        }

        if (Tag.class.isAssignableFrom( var.getClass().getComponentType())) {
            StringBuffer ret = new StringBuffer("");

            for (Tag tag : (Tag[])var) {
                if (tag.kind().equals("Text")) {
                    ret.append(tag.text());
                } else if (tag.kind().equals("@code")) {
                    ret.append("<literal>" + tag.text() + "</literal>");
                } else {
                    throw new TemplateSyntaxException(interpreter, getName(), "Unknown tag type " + tag.kind());
                }
                ret.append(' ');
            }

            if (ret.length() > 0)
                return ret.substring(0, ret.length() - 1);
            else
                return "";
        } else {
            throw new TemplateSyntaxException(interpreter, getName(),
                    "Unknown tag class " + var.getClass().getComponentType().getName());
        }
    }
}
