//
// © ООО «Лаборатория 50» 2019
// DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
//
// This code is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License version 3 only, as
// published by the Free Software Foundation.  Oracle designates this
// particular file as subject to the "Classpath" exception as provided
// by Oracle in the LICENSE file that accompanied this code.
//
// This code is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// version 3 for more details (a copy is included in the СOPYING file that
// accompanied this code).
//
// You should have received a copy of the GNU General Public License version
// 2 along with this work; if not, write to the Free Software Foundation,
// Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
//
package net.lab50.jinjadoclet;

import java.util.Arrays;
import java.util.Optional;

public enum Option {
    INVALID(0, ""),
    OUT(2, "-out"),
    TEMPLATE( 2, "-template");

    private final int length;
    private final String option;

    private Option(int length, String option) {
        this.length = length;
        this.option = option;
    }

    public int getLength() {
        return length;
    }

    public String getOption() {
        return option;
    }

    public Optional<String[]> getOptional(String[][] options) {
        return Arrays.asList(options).stream().filter(o -> option.equals(o[0])).findAny();
    }

}
