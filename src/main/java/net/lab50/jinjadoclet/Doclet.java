//
// © ООО «Лаборатория 50» 2019
// DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
//
// This code is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License version 3 only, as
// published by the Free Software Foundation.  Oracle designates this
// particular file as subject to the "Classpath" exception as provided
// by Oracle in the LICENSE file that accompanied this code.
//
// This code is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// version 3 for more details (a copy is included in the СOPYING file that
// accompanied this code).
//
// You should have received a copy of the GNU General Public License version
// 2 along with this work; if not, write to the Free Software Foundation,
// Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
//
package net.lab50.jinjadoclet;

import com.google.common.base.Charsets;
import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import com.hubspot.jinjava.Jinjava;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.RootDoc;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;

public class Doclet {
    public static int optionLength(String option) {
        return Arrays.asList(Option.values()).stream().filter(o -> o.getOption().equals(option)).findFirst().orElse(Option.INVALID).getLength();
    }

    public static boolean start(RootDoc rootDoc) throws IOException {
        Optional<String[]> optional = Option.OUT.getOptional(rootDoc.options());
        Optional<String[]> tmplfile = Option.TEMPLATE.getOptional(rootDoc.options());

        Jinjava jinjava = new Jinjava();
        jinjava.getGlobalContext().registerFilter(new TagFilter());

        Map<String, Object> context = Maps.newHashMap();
        context.put("package", rootDoc);
        context.put("classes", rootDoc.classes());
        String template = Resources.toString(Resources.getResource(tmplfile.get()[1]), Charsets.UTF_8);
        BufferedWriter writer = new BufferedWriter(new FileWriter(optional.get()[1]));
        writer.write(jinjava.render(template, context));
        writer.close();
        return true;
    }

    public static boolean validOptions(String[][] options, DocErrorReporter reporter) {
        return true;
    }

}
